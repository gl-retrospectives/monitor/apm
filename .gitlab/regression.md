## Summary

<!--- A brief summary of what happened, which feature was affected...  --->

## Impact & Metrics

Start with the following:

| Question | Answer |
| ----- | ----- |
| What was the impact | <!--- service outage, exposure of sensitive data, broken functionality ---> |
| Who was impacted | <!--- (i.e. external customers, internal customers, specific teams, ...) ---> |
| How did this impact customers | <!-- (i.e. preventing them from doing X, incorrect display of Y, ...) ---> |


## Screenshots

<!--- Provide any relevant screenshots or videos that could help understand the incident and its dynamics. --->

## Detection & Response

Start with the following:

| Question | Answer |
| ----- | ----- |
| How long did it take from detection to remediation? |  |
| What steps were taken to remediate? |  |


## What went well

<!--- Identify the things that worked well or as expected. --->


## What can be improved

<!--- What can be improved to prevent this from happening again. --->


## Corrective actions

<!--- List issues (more test coverage, fixes ...) that have been created as corrective actions from this incident. --->


/confidential
/label ~RCA
/label ~devops::monitor
/label ~group::apm
